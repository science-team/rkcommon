#!/bin/sh
# Build and run a program against librkcommon, to verify that the
# headers and library files are installed correctly.
# Author: Francois Mazen <mzf@debian.org>

set -e

cd $AUTOPKGTEST_ARTIFACTS

# Create the program.
cat <<EOF > rkcommon_test.cpp
#include "rkcommon/containers/FlatMap.h"
#include "rkcommon/utility/ParameterizedObject.h"
#include <iostream>
#include <string>
int main(int argc, char *argv[])
{
    // Use FlatMap to check template availability.
    rkcommon::containers::FlatMap<std::string, int> fm;
    fm["first"]  = 1;
    fm["second"] = 2;
    for(auto& it : fm)
    {
        std::cout << it.first << ":" << it.second << std::endl;
    }

    // Use ParameterizedObject to check correct library linking.
    rkcommon::utility::ParameterizedObject obj;
    obj.setParam("third", 3);
    if(!obj.hasParam("third"))
    {
        std::cout << "FAIL: Expected find param third." << std::endl;
        return 1;
    }

    return 0;
}
EOF

# Build the program
echo "building..."
g++ -o rkcommontest rkcommon_test.cpp -lrkcommon
echo "build: OK"

# Run the program
echo "running..."
test -x rkcommontest
./rkcommontest
echo "run: OK"
